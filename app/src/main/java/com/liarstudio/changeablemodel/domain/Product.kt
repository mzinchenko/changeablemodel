package com.liarstudio.changeablemodel.domain

import android.accounts.AuthenticatorDescription
import java.io.Serializable

data class Product(val name: String,
                   var description: String,
                   var price: Int,
                   var fromWarehouse: Boolean) : Serializable