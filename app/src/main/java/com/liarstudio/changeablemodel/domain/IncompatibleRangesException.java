package com.liarstudio.changeablemodel.domain;

/**
 * несовместимые блоки данных
 */
public class IncompatibleRangesException extends IllegalArgumentException {

    public IncompatibleRangesException(String s) {
        super(s);
    }
}
