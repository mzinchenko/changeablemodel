package com.liarstudio.changeablemodel.ui.screen.recycler.controller

import android.view.ViewGroup
import android.widget.TextView
import com.liarstudio.changeablemodel.R
import com.liarstudio.changeablemodel.domain.Product
import com.liarstudio.changeablemodel.ui.base.recycler.controller.BindableItemController
import com.liarstudio.changeablemodel.ui.base.recycler.holder.BindableViewHolder

class ProductListController: BindableItemController<Product, ProductListController.Holder>() {

    override fun createViewHolder(parent: ViewGroup): Holder = Holder(parent)

    override fun getItemId(data: Product): Long = data.name.hashCode().toLong()

    inner class Holder(parent: ViewGroup) :
            BindableViewHolder<Product>(parent, R.layout.product_list_item) {

        private val nameTv: TextView
        private val descTv: TextView
        private val priceTv: TextView
        private val fromWHTv: TextView
        init {
            nameTv = itemView.findViewById(R.id.product_list_name_tv)
            descTv = itemView.findViewById(R.id.product_list_description_tv)
            priceTv = itemView.findViewById(R.id.product_list_price_tv)
            fromWHTv = itemView.findViewById(R.id.product_list_from_wh_tv)
        }

        override fun bind(data: Product) {
            nameTv.text = data.name
            descTv.text = data.description
            priceTv.text = "${data.price}p."
            fromWHTv.text = if (data.fromWarehouse) "Со склада" else "В магазине"
        }
    }
}