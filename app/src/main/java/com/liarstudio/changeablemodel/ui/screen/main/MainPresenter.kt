package com.liarstudio.changeablemodel.ui.screen.main

import android.content.Intent
import com.liarstudio.changeablemodel.domain.Product
import com.liarstudio.changeablemodel.ui.base.ChangeableData
import com.liarstudio.changeablemodel.ui.base.ErrorState
import com.liarstudio.changeablemodel.ui.base.ErrorStateController
import com.liarstudio.changeablemodel.ui.base.navigation.EXTRA_FIRST
import com.liarstudio.changeablemodel.ui.screen.recycler.ProductListActivity

/**
 * Класс главного презентера
 */
class MainPresenter(val view: MainActivity, val route: MainRoute){

    private val screenModel: MainScreenModel

    init {
        screenModel = MainScreenModel(
                ErrorStateController { view.setButtonEnabled(it == ErrorState.NONE) },
                ChangeableData(route.name, view::setName, view::setNameError),
                ChangeableData(route.description, view::setDesc, view::setDescError),
                ChangeableData(route.price, view::setPrice, view::setPriceError),
                ChangeableData(route.fromWarehouse, view::setFromWarehouse)
        )
    }

    /**
     * Эмуляция onLoad из MVP-ядра. Запускается после создания Activity
     */
    fun onLoad(viewRecreated: Boolean) {
        screenModel.refresh()
    }

    /**
     * Метод, срабатывающий при изменении текста EditText name
     * @param text - измененный текст
     */
    fun onNameTextChanged(text: String) { screenModel.name = text }

    /**
     * Метод, срабатывающий при изменении текста EditText desc
     * @param text - измененный текст
     */
    fun onDescTextChanged(text: String) { screenModel.description = text }

    /**
     * Метод, срабатывающий при изменении текста EditText price
     * @param text - измененный текст
     */
    fun onPriceTextChanged(text: String) { screenModel.price = text }

    fun onWarehouseCbChanged(checked: Boolean) { screenModel.fromWarehouse = checked }

    fun onAddClicked() {
        val intent = Intent(view, ProductListActivity::class.java).apply {
            with(screenModel) {
                putExtra(EXTRA_FIRST, Product(name, description, price.toInt(), fromWarehouse))
            }
        }
        view.startActivity(intent)
    }
}