package com.liarstudio.changeablemodel.ui.screen.recycler

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Button
import com.liarstudio.changeablemodel.R
import com.liarstudio.changeablemodel.domain.Product
import com.liarstudio.changeablemodel.ui.base.recycler.EasyAdapter
import com.liarstudio.changeablemodel.ui.base.recycler.ItemList
import com.liarstudio.changeablemodel.ui.screen.recycler.controller.ProductListController

class ProductListActivity : AppCompatActivity() {

    private lateinit var presenter: ProductListPresenter

    private lateinit var addBtn: Button
    private lateinit var recyclerView: RecyclerView
    private lateinit var adapter: EasyAdapter
    private lateinit var productListController: ProductListController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_product_list)
        findViews()
        initListeners()
        initRecycler()

        presenter = ProductListPresenter(this,
                ProductListRoute(intent))
    }

    override fun onResume() {
        super.onResume()
        presenter.onLoad(false)
    }

    fun setRecycler(products: List<Product>) {
        val items = ItemList()
        items.addAll(products, productListController)
        adapter.setItems(items)
    }

    private fun findViews() {
        addBtn = findViewById(R.id.product_list_add_btn)
        recyclerView = findViewById(R.id.recycler)
    }

    private fun initListeners() {
        addBtn.setOnClickListener { presenter.onAddClick() }
    }

    private fun initRecycler() {
        productListController = ProductListController()

        adapter = EasyAdapter()

        val itemAnimator = DefaultItemAnimator()
        itemAnimator.supportsChangeAnimations = false

        recyclerView.itemAnimator = itemAnimator
        recyclerView.layoutManager = LinearLayoutManager(this)
        recyclerView.setHasFixedSize(true)

        recyclerView.adapter = adapter
    }
}
