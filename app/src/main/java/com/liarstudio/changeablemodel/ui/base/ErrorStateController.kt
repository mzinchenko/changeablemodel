package com.liarstudio.changeablemodel.ui.base

/**
 * Класс, показывающий, какие элементы находятся в состоянии ошибки
 * В нашем случае состояния всего два - ERROR (ошибка есть) и NONE (ошибки нет).
 * Однако, этих состояний может быть куда больше.
 * Сделано это для того, чтобы создать один контроллер, отвечающий за всю валидацию.
 */
enum class ErrorState {
    NONE, //Ошибок нет
    ERROR //Есть ошибка
}

/**
 * Контроллер, отвечающий за отображение состояния ошибки.
 * Вызывает listener, внутри которого может быть описана логика
 * дополнительной валидации для отдельных полей.
 * Например, добавление ErrorState.PARTIAL может быть оправдано, когда при неверном вводе
 * некоторых из полей, опциональные поля становятся неактивными,
 * однако мы все равно можем передать необходимую информацию дальше.
 */
 class ErrorStateController(private val errorStateListener: (state: ErrorState) -> Unit) {
    private var state = ErrorState.NONE

    fun setErrorState(state: ErrorState) {
        if (this.state != state) {
            this.state = state
            errorStateListener.invoke(state)
        }
    }
}