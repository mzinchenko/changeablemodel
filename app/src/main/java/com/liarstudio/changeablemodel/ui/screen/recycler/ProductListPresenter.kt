package com.liarstudio.changeablemodel.ui.screen.recycler

import com.liarstudio.changeablemodel.domain.DataList
import com.liarstudio.changeablemodel.domain.Product
import com.liarstudio.changeablemodel.ui.base.ChangeableData

class ProductListPresenter(val view: ProductListActivity, val route: ProductListRoute) {

    private val screenModel: ProductListScreenModel

    init {
        screenModel = ProductListScreenModel(ChangeableData(DataList(route.dataList, 1,1), view::setRecycler))
    }

    /**
     * Эмуляция onLoad из MVP-ядра. Запускается после создания Activity
     */
    fun onLoad(viewRecreated: Boolean) {
        screenModel.refresh()
    }

    /**
     * Функция добавления нового продукта
     */
    fun onAddClick() {
        val productArray = screenModel.products.toTypedArray()
        val dataList = DataList<Product>(
                listOf(Product("name ${productArray.size}", "desc", 0, false)),
                screenModel.products.nextPage, 1)
        screenModel.products.merge(dataList)
        screenModel.refresh()
    }
}

