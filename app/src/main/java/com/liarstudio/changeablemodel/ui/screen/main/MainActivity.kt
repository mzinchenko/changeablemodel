package com.liarstudio.changeablemodel.ui.screen.main

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import com.liarstudio.changeablemodel.R

/**
 * Класс основного представления
 */
class MainActivity : AppCompatActivity() {

    private lateinit var nameEt: EditText
    private lateinit var descEt: EditText
    private lateinit var priceEt: EditText
    private lateinit var addBtn: Button
    private lateinit var fromWarehouseCb: CheckBox

    private lateinit var presenter: MainPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        findViews()
        presenter = MainPresenter(this,
                MainRoute(name = "Имя",
                        description = "Описание",
                        price = "500",
                        fromWarehouse = true)
        )
        initListeners()
    }

    override fun onResume() {
        super.onResume()
        //Запускаем onLoad, когда Activity уже создана
        presenter.onLoad(false)
    }

    /**
     * Метод, срабатывающий при ошибке ввода названия
     */
    fun setNameError() {
        nameEt.error = getString(R.string.error_invalid_input)
    }

    /**
     * Метод, срабатывающий при ошибке ввода описания
     */
    fun setDescError() {
        descEt.error = getString(R.string.error_invalid_input)
    }

    /**
     * Метод, срабатывающий при ошибке ввода цены
     */
    fun setPriceError() {
        priceEt.error = getString(R.string.error_invalid_input)
    }

    /**
     * Метод изменения названия
     * @param name - название для изменения
     */
    fun setName(name: String) {
        if (nameEt.text.toString() != name) {
            nameEt.setText(name)
        }
    }

    /**
     * Метод изменения описания
     * @param desc - описание для изменения
     */
    fun setDesc(desc: String) {
        if (descEt.text.toString() != desc) {
            descEt.setText(desc)
        }
    }

    /**
     * Метод изменения цены
     * @param address - цена для изменения
     */
    fun setPrice(address: String) {
        if (priceEt.text.toString() != address) {
            priceEt.setText(address)
        }
    }

    /**
     * Метод состояния местонахождения товара
     * @param address - цена для изменения
     */
    fun setFromWarehouse(fromWarehouse: Boolean) {
        if (fromWarehouseCb.isChecked != fromWarehouse) {
            fromWarehouseCb.isChecked = fromWarehouse
        }
    }

    /**
     * Метод, срабатывающий при изменении состояния ошибки
     * @param enabled - состояние кнопки
     */
    fun setButtonEnabled(enabled: Boolean) {
        addBtn.isEnabled = enabled
    }

    private fun findViews() {
        nameEt = findViewById(R.id.main_name_et)
        descEt = findViewById(R.id.main_desc_et)
        priceEt = findViewById(R.id.main_price_et)
        fromWarehouseCb = findViewById(R.id.main_warehouse_cb)
        addBtn = findViewById(R.id.main_add_btn)
    }

    private fun initListeners() {
        nameEt.afterTextChanged { presenter.onNameTextChanged(it) }
        descEt.afterTextChanged { presenter.onDescTextChanged(it) }
        priceEt.afterTextChanged { presenter.onPriceTextChanged(it) }
        fromWarehouseCb.setOnCheckedChangeListener {_, isChecked ->
            presenter.onWarehouseCbChanged(isChecked)}
        addBtn.setOnClickListener { presenter.onAddClicked() }
    }
}

/**
 * Extension-функция для упрощения работы с текстом.
 * Чтобы не вызывать несколько раз TextWatcher, заменим его на функцию
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
        }

        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }
    })
}
