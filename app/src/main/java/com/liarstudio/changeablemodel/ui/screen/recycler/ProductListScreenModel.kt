package com.liarstudio.changeablemodel.ui.screen.recycler

import com.liarstudio.changeablemodel.domain.DataList
import com.liarstudio.changeablemodel.domain.Product
import com.liarstudio.changeablemodel.ui.base.ChangeableData
import com.liarstudio.changeablemodel.ui.base.ScreenModel

class ProductListScreenModel(var changeableProducts: ChangeableData<DataList<Product>>
) : ScreenModel() {

    var products: DataList<Product>
        get() = changeableProducts.data
        set(value) = setField(value, changeableProducts)

    override fun refresh() {
        changeableProducts.onChanged(changeableProducts.data)
    }
}