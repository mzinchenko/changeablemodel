package com.liarstudio.changeablemodel.ui.screen.main

import com.liarstudio.changeablemodel.ui.base.ChangeableData
import com.liarstudio.changeablemodel.ui.base.ErrorState
import com.liarstudio.changeablemodel.ui.base.ErrorStateController
import com.liarstudio.changeablemodel.ui.base.ScreenModel

/**
 * Класс главной модели
 *
 * Содержит логику проверки, отображения и изменения данных
 * Все изменения происходят с помощью метода setField
 * @param errorStateController - контроллер для отслеживания изменений.
 * Расположен контроллер в модели, а не внутри полей ChangeableData, так как
 * задействованная в нем логика охватывает всю модель, или ее связанные части, а не отдельное поле
 * @param changeableName - поле для хранения названия, а так же listener'ов на изменение и ошибку
 * @param changeableDesc - поле для хранения описания, а так же listener'ов на изменение и ошибку
 * @param changeablePrice - поле для хранения адреса, а так же listener'ов на изменение и ошибку
 */
class MainScreenModel(private val errorStateController: ErrorStateController,
                      private val changeableName: ChangeableData<String>,
                      private val changeableDesc: ChangeableData<String>,
                      private val changeablePrice: ChangeableData<String>,
                      private val changeableFromWH: ChangeableData<Boolean>
) : ScreenModel() {
    var name: String
        get() = changeableName.data
        set(value) = setField(value, changeableName, this::checkName)

    var description: String
        get() = changeableDesc.data
        set(value) = setField(value, changeableDesc, this::checkDescription)

    var price: String
        get() = changeablePrice.data
        set(value) = setField(value, changeablePrice, this::checkPrice)

    var fromWarehouse: Boolean
        get() = changeableFromWH.data
        set(value) = setField(value, changeableFromWH)

    init {
        name = changeableName.data
        description = changeableDesc.data
        price = changeablePrice.data
        fromWarehouse = changeableFromWH.data
    }


    /**
     * Принудительное обновление view - вызываем listener'ы с текущими значениями данных
     */
    override fun refresh() {
        changeableName.onChanged(changeableName.data)
        changeableDesc.onChanged(changeableDesc.data)
        changeablePrice.onChanged(changeablePrice.data)
        changeableFromWH.onChanged(changeableFromWH.data)
    }

    private fun <T> setField(data: T,
                             field: ChangeableData<T>,
                             checkPredicate: () -> Boolean) {
        field.data = data
        field.onChanged(data)
        if (checkPredicate()) {
            errorStateController.setErrorState(
                    if (checkAll())
                        ErrorState.NONE
                    else
                        ErrorState.ERROR)
        } else {
            field.onError()
            errorStateController.setErrorState(ErrorState.ERROR)
        }
    }

    private fun checkAll() = checkName() && name.isNotEmpty() &&
            checkDescription() && description.isNotEmpty() &&
            checkPrice() && price.isNotEmpty()

    private fun checkName() = name.all(Char::isLetter)
    private fun checkDescription() = description.length <= 50
    private fun checkPrice() = price.all(Char::isDigit)

}