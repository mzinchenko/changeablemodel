package com.liarstudio.changeablemodel.ui.base

abstract class ScreenModel {

    protected fun <T>setField(data: T, field: ChangeableData<T>) {
        field.data = data
        field.onChanged(data)
    }

    abstract fun refresh()
}