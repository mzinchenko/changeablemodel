package com.liarstudio.changeablemodel.ui.screen.main

/**
 * Класс главного маршрута
 */
class MainRoute {
    val name: String
    val description: String
    val price: String
    val fromWarehouse: Boolean

    constructor() {
        name = ""
        description = ""
        price = ""
        fromWarehouse = false
    }

    constructor(name: String, description: String, price: String, fromWarehouse: Boolean) {
        this.name = name
        this.description = description
        this.price = price
        this.fromWarehouse = fromWarehouse
    }
}