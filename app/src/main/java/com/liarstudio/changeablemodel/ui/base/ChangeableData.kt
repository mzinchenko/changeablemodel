package com.liarstudio.changeablemodel.ui.base

/**
 * Класс изменяемых данных
 * Содержит поля
 * @param data - изменяемые данные
 * @param onChanged - listener, срабатывающий при изменении элемента
 * @param onError - listener, срабатывающий при возникновении ошибки
 */
class ChangeableData<T>(var data: T,
                        val onChanged: (data: T) -> Unit,
                        val onError: () -> Unit) {

    constructor(data: T, onChanged: (data: T) -> Unit) : this(data, onChanged, {})
}