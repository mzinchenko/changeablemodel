package com.liarstudio.changeablemodel.ui.screen.recycler

import android.content.Intent
import com.liarstudio.changeablemodel.domain.Product
import com.liarstudio.changeablemodel.ui.base.navigation.EXTRA_FIRST

class ProductListRoute {

    var dataList: List<Product>

    constructor() {
        dataList = listOf(Product(name = "",
                description = "",
                price = 0,
                fromWarehouse = false))
    }

    constructor(name: String, description: String, price: String, fromWarehouse: Boolean) {
        dataList = listOf(Product(name, description, price.toInt(), fromWarehouse))
    }
    constructor(intent: Intent) {
        dataList = listOf(intent.getSerializableExtra(EXTRA_FIRST) as Product)
    }
}